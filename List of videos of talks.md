* Timothy Gowers [Perverse Incentives](https://mediasite.uit.no/Mediasite/Play/db5614d2d8de4b528b62929b5209355d1d?PlayFrom=2400000) Norway November 2017 - [similar talk June 2017 in Brisbane](https://www.youtube.com/watch?v=OqG_jzE1tm4)
* [Jeffrey Mackie-Mason](http://www.msri.org/general_events/21641) at Mathematical Science Research Institute (Berkeley) Sponsors Day March 2018 (go to Future Panel, item 5)
* Victor Reiner [discussing](http://www.msri.org/general_events/21641) experiences in flipping J. Algebraic Combinatorics (right after Mackie-Mason talk, Future Panel)
* Kathleen Shearer [Sustainability and Innovation in Scholarly Communication](https://www.infodocket.com/2018/04/12/video-kathleen-shearers-keynote-presentation-sustainability-and-innovation-in-scholarly-communication-at-rluk-2018-now-available-online/) at RLUK 2018, April 2018
* Danny Kingsley [Is the tail wagging the dog? Perversity in academic rewards](http://coaspvideos.org/2016/videos/play/1401) COASP 2017, Lisbon, Portugal 20-21 September 2017 
* Danny Kingsley [Reward, reproducibility and recognition in research - the case for going Open](http://septentrio.uit.no/index.php/SCS/article/view/4036) Norway November 2016
* Danny Kingsley [Emerging from the Chrysalis - Transforming Libraries for the Future](https://www.youtube.com/watch?v=kplRbU2ZE-U&list=PLp6Pui4M5LhElDR2ERl9_5D5nJoksYw3e&index=2&t=1349s) Athlone, Ireland, May 2017
* Piled Higher and Deeper (PHD Comics) [Open Access Explained](https://www.youtube.com/watch?v=L5rVH1KGBCY)
* Kathleen Shearer [Open is not enough (COAR2018)] (https://www.youtube.com/watch?feature=youtu.be&v=qV1rxxZDYRA)
* Movie: [Paywall, the business of scholarship] (https://youtu.be/HM_nWsdbNvQ)

