# Journal licences

The third [Fair Open Access Principle](www.fairopenaccess.org/) requires that the journal use an explicit open access licence. There are several reasons. Licences protect authors, and simplify getting permission for fair use of published work (for example for your work to be used in Wikipedia, reuse rights must be made explicit). 


## Options

* *Minimum* - Custom licences. 

These is quite common in independent journals who wish to allow anyone to read papers freely. Before CC licences were formulated, this was common practice. Many of these licences contain copyright transfer claims that are not only internally inconsistent (or at least confusing) but they are inconsistent with the second Principle of Fair Open Access. Choosing a well-known, industry standard and thoroughly tested licence makes it easier for authors, readers and publishers.

* *Good* - [Creative Commons (CC) licenses](https://creativecommons.org/licenses/). 
   * [CC BY](https://creativecommons.org/licenses/by/4.0/) (Attribution) - Work is free to share and adapt, but reuser must give appropriate credit, provide a link to the license, and indicate if changes were made, and must not add additional restrictions.  ![Image](https://i.creativecommons.org/l/by/4.0/88x31.png?raw=true)
   * [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) (Attribution-ShareAlike) - Like CC BY, but any work derived from the original must also be licenced under CC BY-SA. ![Image](https://i.creativecommons.org/l/by-sa/4.0/88x31.png?raw=true)
   * [CC BY-ND](https://creativecommons.org/publicdomain/by-nd/4.0/) (Attribution-NoDerivs) - Like with CC BY work is free to share, but nothing derived from it may be distributed. ![Image](https://i.creativecommons.org/l/by-nd/4.0/88x31.png?raw=true)
   * [CC BY-NC](https://creativecommons.org/publicdomain/by-nc/4.0/) (Attribution-NonCommercial) - Like CC BY, but work may not be used for commercial purposes.  ![Image](https://i.creativecommons.org/l/by-nc/4.0/88x31.png?raw=true)
   * Combinations [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) and [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/).
   * [CC0](https://creativecommons.org/publicdomain/zero/1.0/) (Zero) - Work is put into the public domain. ![Image](https://i.creativecommons.org/p/zero/1.0/88x31.png?raw=true)

The FAQ below gives pros and cons of the different licences, including downsides of the NC and ND clauses. Which is why we recommend the following:

* *Best* - CC licences [approved for free cultural works](https://creativecommons.org/share-your-work/public-domain/freeworks/) like CC BY (but also CC0 and BY-SA).
![Image](https://i.creativecommons.org/l/by/4.0/88x31.png?raw=true)

Ideally the licence should be displayed on the published work and on the journal website. It is advisable to adopt the latest CC licence version 4.0. Also see the [CC downloads](https://creativecommons.org/about/downloads/) for logos.


## FAQ

##### **Q**. Why have a licence at all?

**A**. As author and as publisher, but also as reader or crawler (or other stakeholder) you want to know your rights with regards to papers. (Can I upload the paper on my homepage? Can I reuse it my thesis or book? Can I add the figure of the paper to a Wikipedia article?) Simply putting papers on the journal homepage and then letting everyone guess what the rights are or having to ask for clarifications and permissions is not a feasible option. A licence clarifies this. It should thus be made clearly on the journal homepage and on each paper (one might get a paper through other ways then the journal homepage).
Having a licence is also something people have agreed on with the Fair Open Access Principle (#3: *All articles are published open access and an explicit open access licence is used.*)

#####  **Q**. Why not just allow everyone to read articles freely?

**A**. Everyone agrees that at a minimum, open access means that the articles are free to read to anyone anywhere, without any technical barriers such as requiring registration.
Beyond that, there are different definitions. The three classic declarations on Open Access ([Budapest](http://budapestopenaccessinitiative.org/), [Berlin](https://openaccess.mpg.de/Berlin-Declaration), [Bethesda](http://legacy.earlham.edu/~peters/fos/bethesda.htm)) all agree that OA also includes the right to re-use content.
* The [Budapest](http://budapestopenaccessinitiative.org/) statement says *“[...] free availability on the public internet, permitting any users to read, download, copy, distribute, print, search, or link to the full texts of these articles, crawl them for indexing, pass them as data to software, or use them for any other lawful purpose, without financial, legal, or technical barriers other than those inseparable from gaining access to the internet itself”*.
* The [Berlin](https://openaccess.mpg.de/Berlin-Declaration) and the [Bethesda](http://legacy.earlham.edu/~peters/fos/bethesda.htm) statement say *"[...] grant(s) to all users a free, irrevocable, worldwide, right of access to, and a license to copy, use, distribute, transmit and display the work publicly and to make and distribute derivative works, in any digital medium for any responsible purpose [...]"*. 


##### **Q**. What licences are currently used by free journals?

**A**. In June 2018 Free Journal Network collected the following data from its members: CC BY 30, custom 6, CC BY-NC-ND 2, CC BY-SA 1. Many older journals still have custom licences.

Many people who have put the most thought into open-access licences have come down on the side of CC BY. These include respected OA publishers both commercial and non-profit (BioMed Central, PLOS, PeerJ), funding charities (Wellcome Trust, Gates Foundation) and national bodies (RCUK). As a result, journals which publish open access materials under more restrictive licences will not be acceptable venues for research funded by the Wellcome Trust, Gates Foundation, etc.


##### **Q**. Why add the NC (NonCommercial) clause, why not?

**A**. Some people feel anxiety about their work being used in ways they do not approve of, and especially of the possibility that others may make money from it without their getting a cut. For this reason, they may prefer a more restrictive licence such as Creative Commons Attribution-NonCommercial (CC BY-NC).
However, this licence prevents a large and fuzzy-bordered set of re-uses. The problem is that no-one knows exactly what is and isn’t allowed under the terms “non-commercial” — it’s a thing that can be decided only in court, and on a jurisdiction-by-jurisdiction basis. For example, it may be that CC BY-NC materials can’t be used in teaching in a university, because the university charges tuition fees — an outcome that surely is not what any scholar intends for their OA works. A court in Germany ruled that any non-personal use counts as “commercial” even when no money changes hands: under that interpretation no CC BY-NC works could ever be used in any kind of teaching.


##### **Q**. Why add the ND (NonDeriv) clause, why not?

**A**. Some people are concerned about distortion of their work, and hence prefer CC BY-ND. This means that re-use of any work is only allowed if not altered in any way (formatting, content, joined publication, etc.). When arXiv puts his stamp on a paper, this would count as a derivative. Another problem with ND is that derivative works are essential for Wikipedia (for example, including a proof of a theorem from your paper but with different notation, or redrawing graphs).

Also, note that the ND clause has nothing todo with allowing or not allowing plagiarism. Quoting Martin Paul Eve from [here](https://www.martineve.com/2012/12/20/on-that-statement-by-history-journal-editors/): *"Plagiarism is not a legal term that has any bearing upon licensing; it is a term from within the academy. In this case, copyright infringement would be unlikely; after all, the point of a CC license is to facilitate sharing. Plagiarism, conversely, could happen, but this would be nothing to do with a CC-BY license; it is a matter for the academic and their institution."* (And see [this comment](https://gitlab.com/publishing-reform/discussion/issues/58#note_73054237).)


##### **Q**. Why add the SA (ShareAlike) clause, why not?

**A**. Some people want to force openness on anyone using their work, and hence use CC BY-SA. Problems can arise from licence compatibility problem. Also CC BY-SA 4.0 is not compatible with Wikipedia (see [here](https://en.wikipedia.org/wiki/Wikipedia:FAQ/Copyright#Can_I_add_something_to_Wikipedia_that_I_got_from_somewhere_else?)), only the older versions CC BY-SA 1.0, 2.0, 2.5, 3.0 are.


##### **Q**. Can exceptions be made to the general licence?

**A**. The copyright holder (which should be the author) can waive any restrictive clause if he or she wants to. So, an author could waive for the journal the ND clause to enable it to publish a revised version, if so needed.


##### **Q**. What are some traps to avoid?

**A**. The following remarks are based on mistakes made by actual journals.

* Once a paper is published under a CC licence (e.g. CC BY, CC BY-NC, CC BY-ND), a journal or publisher cannot retain exclusive diffusion rights. The very meaning of these licences is that the authors (and actually anybody) can legally put the publisher-formatted pdf file on a web page, the authors can put the publisher pdf on HaL and the arXiv, etc.

* Having a notice on the journal's web page that contradicts the licence is a significant issue, as it works against the point of CC of making clear what is authorized and what is not -- and this is a big part of Open Access. Thus, if articles are stamped e.g. CC BY-ND the journal should not make false claims such as *"The journal reserve for themselves the exclusive right to publish the texts as laid out in the specific format of the Journal"*.

* A claim such as *"the authors are allowed to leave their original source files in free access on their private home page or servers"* is logically correct, but misleading: under CC BY (with or without ND, NC clauses) the authors have also the right to post the formatted article, and mentioning a more restricted right casts doubt.


## Credit
Some text is based on contributions by Mike Taylor and Benoit Kloeckner, see also the [discussion](https://gitlab.com/publishing-reform/discussion/issues/43).
