# Forming a team, contact with scholarly community

The [1. Fair Open Access Principle](https://www.fairopenaccess.org/) says:
> 1. The journal has a transparent ownership structure, and is controlled by and responsive to the scholarly community.

It is worth considering the last part (*responsive to the scholarly community*) right from the start. 
It is unrealistic to create a journal for a community one doesn't know. On the other hand, a journal where the community knows about the plan to start a new fair OA journal and, furthermore, is involved from the start will surely find more acceptance. Contacts and visibility in the field are indispensable assets to get people on board.

We see the journal [Quantum](http://quantum-journal.org/) as a prime example in this matter. The following describes some actions they took (based on a comment by Christopher Gogolin):
* Creating a "pamphlet" describing what seems wrong with the current status quo and what the plans to change it are.
* Contacting important people in the field and asking them whether they would join the Steering Board (SB), help deciding on the concrete policies and would lend their name.
* Setting up a preliminary website and blog to inform the community that something was happening. At this stage already big names and design are super important. People have to believe that it will be a success, otherwise there is no chance. 
* Creating visibility on social media (definitely Facebook and Twitter, maybe others like LinkedIn) by posting articles on the problems with the current publishing industry.
* Inviting the community to discuss the policies they were proposing together with the SB. This happened on reddit https://www.reddit.com/r/quantumjournal/ (and they are still using this platform). They discussed pretty fundamental questions and were really open to make big changes to their plan like: "Should Quantum be selective?"
* Using the communication channel for a call for editors, letting the SB select the board from the applicants. One needs highly motivated people who are willing to work for free so it is best to recruit the board from researchers who are willing to sign up for this voluntarily, rather than approach them. People will find it hard to say no, but might then not be super motivated.
