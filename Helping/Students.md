# How students can help

- Inform yourself
- Start new threads on what concerns you
- Subscribe to other threads and be among the first informed when changes happen
- Attract active passionate people, start new projects and find collaborators
- Take part in action and share your ideas and feedback.